<?php

/**
 * Utilisations de pipelines par La Fabrique - Test objet-enfant
 *
 * @plugin     La Fabrique - Test objet-enfant
 * @copyright  2024
 * @author     bricebou
 * @licence    GNU/GPL
 * @package    SPIP\Devof\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Ajouter les objets sur les vues des parents directs
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function devof_affiche_enfants($flux) {
	if (
		$e = trouver_objet_exec($flux['args']['exec'])
		and $e['edition'] === false
	) {
		$id_objet = $flux['args']['id_objet'];

		if ($e['type'] === 'rubrique') {
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/trucs',
				[
					'titre' => _T('truc:titre_trucs_rubrique'),
					'id_rubrique' => $id_objet,
					'tri_liste_trucs' => _request('tri_liste_trucs'),
					'debut_liste_trucs' => _request('debut_liste_trucs'),
				]
			);

			if (autoriser('creertrucdans', 'rubrique', $id_objet)) {
				include_spip('inc/presentation');
				$flux['data'] .= icone_verticale(
					_T('truc:icone_creer_truc'),
					generer_url_ecrire('truc_edit', "id_rubrique=$id_objet"),
					'truc-24.png',
					'new',
					'right'
				) . "<br class='nettoyeur' />";
			}
		}
	}
	return $flux;
}

/**
 * Afficher le nombre d'éléments dans les parents
 *
 * @pipeline boite_infos
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function devof_boite_infos($flux) {
	if (isset($flux['args']['type']) and isset($flux['args']['id']) and $id = intval($flux['args']['id'])) {
		$texte = '';
		if ($flux['args']['type'] == 'rubrique' and $nb = sql_countsel('spip_trucs', ["statut='publie'", 'id_rubrique=' . $id])) {
			$texte .= '<div>' . singulier_ou_pluriel($nb, 'truc:info_1_truc', 'truc:info_nb_trucs') . "</div>\n";
		}
		if ($flux['args']['type'] == 'truc' and $nb = sql_countsel('spip_muches', ["statut='publie'", 'id_truc=' . $id])) {
			$texte .= '<div>' . singulier_ou_pluriel($nb, 'muche:info_1_muche', 'muche:info_nb_muches') . "</div>\n";
		}
		if ($flux['args']['type'] == 'truc' and $nb = sql_countsel('spip_choses', ['id_truc=' . $id])) {
			$texte .= '<div>' . singulier_ou_pluriel($nb, 'chose:info_1_chose', 'chose:info_nb_choses') . "</div>\n";
		}
		if ($texte and $p = strpos($flux['data'], '<!--nb_elements-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		}
	}
	return $flux;
}


/**
 * Compter les enfants d'un objet
 *
 * @pipeline objets_compte_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function devof_objet_compte_enfants($flux) {
	if ($flux['args']['objet'] == 'truc' and $id_truc = intval($flux['args']['id_objet'])) {
		// juste les publiés ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['muches'] = sql_countsel('spip_muches', 'id_truc= ' . intval($id_truc) . " AND (statut = 'publie')");
		} else {
			$flux['data']['muches'] = sql_countsel('spip_muches', 'id_truc= ' . intval($id_truc) . " AND (statut <> 'poubelle')");
		}
	}
	if ($flux['args']['objet'] == 'truc' and $id_truc = intval($flux['args']['id_objet'])) {
		$flux['data']['choses'] = sql_countsel('spip_choses', 'id_truc= ' . intval($id_truc));
	}

	return $flux;
}


/**
 * Optimiser la base de données
 *
 * Supprime les objets à la poubelle.
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function devof_optimiser_base_disparus($flux) {

	sql_delete('spip_trucs', "statut='poubelle' AND maj < " . sql_quote($flux['args']['date']));

	sql_delete('spip_muches', "statut='poubelle' AND maj < " . sql_quote($flux['args']['date']));

	return $flux;
}
