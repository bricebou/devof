<?php

/**
 * Fonctions utiles au plugin La Fabrique - Test objet-enfant
 *
 * @plugin     La Fabrique - Test objet-enfant
 * @copyright  2024
 * @author     bricebou
 * @licence    GNU/GPL
 * @package    SPIP\Devof\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
