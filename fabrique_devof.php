<?php

/**
 *  Fichier généré par la Fabrique de plugin v7
 *   le 2024-02-18 09:25:32
 *
 *  Ce fichier de sauvegarde peut servir à recréer
 *  votre plugin avec le plugin «Fabrique» qui a servi à le créer.
 *
 *  Bien évidemment, les modifications apportées ultérieurement
 *  par vos soins dans le code de ce plugin généré
 *  NE SERONT PAS connues du plugin «Fabrique» et ne pourront pas
 *  être recréées par lui !
 *
 *  La «Fabrique» ne pourra que régénerer le code de base du plugin
 *  avec les informations dont il dispose.
 *
**/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$data = array (
  'fabrique' => 
  array (
    'version' => 7,
  ),
  'paquet' => 
  array (
    'prefixe' => 'devof',
    'nom' => 'La Fabrique - Test objet-enfant',
    'slogan' => '',
    'description' => '',
    'logo' => 
    array (
      0 => '',
    ),
    'credits' => 
    array (
      'logo' => 
      array (
        'texte' => '',
        'url' => '',
      ),
    ),
    'version' => '1.0.0',
    'auteur' => 'bricebou',
    'auteur_lien' => '',
    'licence' => 'GNU/GPL',
    'etat' => 'dev',
    'compatibilite' => '[4.2.9;4.2.*]',
    'documentation' => '',
    'administrations' => 'on',
    'schema' => '1.0.0',
    'formulaire_config' => '',
    'formulaire_config_titre' => '',
    'fichiers' => 
    array (
      0 => 'autorisations',
      1 => 'fonctions',
      2 => 'options',
      3 => 'pipelines',
    ),
    'inserer' => 
    array (
      'paquet' => '',
      'administrations' => 
      array (
        'maj' => '',
        'desinstallation' => '',
        'fin' => '',
      ),
      'base' => 
      array (
        'tables' => 
        array (
          'fin' => '',
        ),
      ),
    ),
    'scripts' => 
    array (
      'pre_copie' => '',
      'post_creation' => '',
    ),
    'exemples' => '',
    'saisies_mode' => 'php',
  ),
  'objets' => 
  array (
    0 => 
    array (
      'nom' => 'Trucs',
      'nom_singulier' => 'Truc',
      'genre' => 'masculin',
      'logo' => 
      array (
        0 => '',
        32 => '',
        24 => '',
        16 => '',
        12 => '',
      ),
      'table' => 'spip_trucs',
      'cle_primaire' => 'id_truc',
      'cle_primaire_sql' => 'bigint(21) NOT NULL',
      'table_type' => 'truc',
      'champs' => 
      array (
        0 => 
        array (
          'nom' => 'Titre',
          'champ' => 'titre',
          'sql' => 'varchar(50) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
      ),
      'champ_titre' => 'titre',
      'champ_date' => '',
      'champ_date_ignore' => 'on',
      'statut' => 'on',
      'chaines' => 
      array (
        'titre_objets' => 'Trucs',
        'titre_page_objets' => 'Les trucs',
        'titre_objet' => 'Truc',
        'info_aucun_objet' => 'Aucun truc',
        'info_1_objet' => 'Un truc',
        'info_nb_objets' => '@nb@ trucs',
        'icone_creer_objet' => 'Créer un truc',
        'icone_modifier_objet' => 'Modifier ce truc',
        'titre_logo_objet' => 'Logo de ce truc',
        'titre_langue_objet' => 'Langue de ce truc',
        'texte_definir_comme_traduction_objet' => 'Ce truc est une traduction du truc numéro :',
        'titre_\\objets_lies_objet' => 'Liés à ce truc',
        'titre_objets_rubrique' => 'Trucs de la rubrique',
        'info_objets_auteur' => 'Les trucs de cet auteur',
        'retirer_lien_objet' => 'Retirer ce truc',
        'retirer_tous_liens_objets' => 'Retirer tous les trucs',
        'ajouter_lien_objet' => 'Ajouter ce truc',
        'texte_ajouter_objet' => 'Ajouter un truc',
        'texte_creer_associer_objet' => 'Créer et associer un truc',
        'texte_changer_statut_objet' => 'Ce truc est :',
        'supprimer_objet' => 'Supprimer cet truc',
        'confirmer_supprimer_objet' => 'Confirmez-vous la suppression de cet truc ?',
      ),
      'rubriques' => 
      array (
        0 => 'id_rubrique',
        1 => 'vue_rubrique',
      ),
      'liaison_directe' => '',
      'table_liens' => '',
      'afficher_liens' => '',
      'roles' => '',
      'auteurs_liens' => '',
      'vue_auteurs_liens' => '',
      'fichiers' => 
      array (
        'echafaudages' => 
        array (
          0 => 'prive/squelettes/contenu/objets.html',
          1 => 'prive/objets/infos/objet.html',
          2 => 'prive/squelettes/contenu/objet.html',
        ),
      ),
      'saisies' => 
      array (
        0 => 'objets',
      ),
      'autorisations' => 
      array (
        'objets_voir' => '',
        'objet_creer' => '',
        'objet_voir' => '',
        'objet_modifier' => '',
        'objet_supprimer' => '',
        'associerobjet' => '',
      ),
      'boutons' => 
      array (
        0 => 'menu_edition',
      ),
    ),
    1 => 
    array (
      'nom' => 'Muches',
      'nom_singulier' => 'Muche',
      'genre' => 'masculin',
      'logo' => 
      array (
        0 => '',
        32 => '',
        24 => '',
        16 => '',
        12 => '',
      ),
      'table' => 'spip_muches',
      'cle_primaire' => 'id_muche',
      'cle_primaire_sql' => 'bigint(21) NOT NULL',
      'table_type' => 'muche',
      'champs' => 
      array (
        0 => 
        array (
          'nom' => 'Titre',
          'champ' => 'titre',
          'sql' => 'varchar(50) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
      ),
      'champ_titre' => 'titre',
      'champ_date' => '',
      'champ_date_ignore' => 'on',
      'statut' => 'on',
      'chaines' => 
      array (
        'titre_objets' => 'Muches',
        'titre_page_objets' => 'Les muches',
        'titre_objet' => 'Muche',
        'info_aucun_objet' => 'Aucun muche',
        'info_1_objet' => 'Un muche',
        'info_nb_objets' => '@nb@ muches',
        'icone_creer_objet' => 'Créer un muche',
        'icone_modifier_objet' => 'Modifier ce muche',
        'titre_logo_objet' => 'Logo de ce muche',
        'titre_langue_objet' => 'Langue de ce muche',
        'texte_definir_comme_traduction_objet' => 'Ce muche est une traduction du muche numéro :',
        'titre_\\objets_lies_objet' => 'Liés à ce muche',
        'titre_objets_rubrique' => 'Muches de la rubrique',
        'info_objets_auteur' => 'Les muches de cet auteur',
        'retirer_lien_objet' => 'Retirer ce muche',
        'retirer_tous_liens_objets' => 'Retirer tous les muches',
        'ajouter_lien_objet' => 'Ajouter ce muche',
        'texte_ajouter_objet' => 'Ajouter un muche',
        'texte_creer_associer_objet' => 'Créer et associer un muche',
        'texte_changer_statut_objet' => 'Ce muche est :',
        'supprimer_objet' => 'Supprimer cet muche',
        'confirmer_supprimer_objet' => 'Confirmez-vous la suppression de cet muche ?',
      ),
      'liaison_directe' => 'spip_trucs',
      'table_liens' => '',
      'afficher_liens' => '',
      'roles' => '',
      'auteurs_liens' => '',
      'vue_auteurs_liens' => '',
      'fichiers' => 
      array (
        'echafaudages' => 
        array (
          0 => 'prive/squelettes/contenu/objets.html',
          1 => 'prive/objets/infos/objet.html',
          2 => 'prive/squelettes/contenu/objet.html',
        ),
        'explicites' => 
        array (
          0 => 'action/supprimer_objet.php',
        ),
      ),
      'autorisations' => 
      array (
        'objets_voir' => '',
        'objet_creer' => '',
        'objet_voir' => '',
        'objet_modifier' => '',
        'objet_supprimer' => '',
        'associerobjet' => '',
      ),
      'boutons' => 
      array (
        0 => 'menu_edition',
      ),
    ),
    2 => 
    array (
      'nom' => 'Choses',
      'nom_singulier' => 'Chose',
      'genre' => 'feminin',
      'logo' => 
      array (
        0 => '',
        32 => '',
        24 => '',
        16 => '',
        12 => '',
      ),
      'table' => 'spip_choses',
      'cle_primaire' => 'id_chose',
      'cle_primaire_sql' => 'bigint(21) NOT NULL',
      'table_type' => 'chose',
      'champs' => 
      array (
        0 => 
        array (
          'nom' => 'Titre',
          'champ' => 'titre',
          'sql' => 'varchar(25) NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
            2 => 'obligatoire',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
      ),
      'champ_titre' => 'titre',
      'champ_date' => '',
      'champ_date_ignore' => '',
      'statut' => '',
      'chaines' => 
      array (
        'titre_objets' => 'Choses',
        'titre_page_objets' => 'Les choses',
        'titre_objet' => 'Chose',
        'info_aucun_objet' => 'Aucune chose',
        'info_1_objet' => 'Une chose',
        'info_nb_objets' => '@nb@ choses',
        'icone_creer_objet' => 'Créer une chose',
        'icone_modifier_objet' => 'Modifier cette chose',
        'titre_logo_objet' => 'Logo de cette chose',
        'titre_langue_objet' => 'Langue de cette chose',
        'texte_definir_comme_traduction_objet' => 'Cette chose est une traduction de la chose numéro :',
        'titre_\\objets_lies_objet' => 'Liés à cette chose',
        'titre_objets_rubrique' => 'Choses de la rubrique',
        'info_objets_auteur' => 'Les choses de cet auteur',
        'retirer_lien_objet' => 'Retirer cette chose',
        'retirer_tous_liens_objets' => 'Retirer toutes les choses',
        'ajouter_lien_objet' => 'Ajouter cette chose',
        'texte_ajouter_objet' => 'Ajouter une chose',
        'texte_creer_associer_objet' => 'Créer et associer une chose',
        'texte_changer_statut_objet' => 'Cette chose est :',
        'supprimer_objet' => 'Supprimer cette chose',
        'confirmer_supprimer_objet' => 'Confirmez-vous la suppression de cette chose ?',
      ),
      'liaison_directe' => 'spip_trucs',
      'table_liens' => '',
      'afficher_liens' => '',
      'roles' => '',
      'auteurs_liens' => '',
      'vue_auteurs_liens' => '',
      'autorisations' => 
      array (
        'objets_voir' => '',
        'objet_creer' => '',
        'objet_voir' => '',
        'objet_modifier' => '',
        'objet_supprimer' => '',
        'associerobjet' => '',
      ),
      'boutons' => 
      array (
        0 => 'menu_edition',
      ),
    ),
  ),
  'images' => 
  array (
    'paquet' => 
    array (
      'logo' => 
      array (
        0 => 
        array (
          'extension' => '',
          'contenu' => '',
        ),
      ),
    ),
    'objets' => 
    array (
      0 => 
      array (
      ),
      1 => 
      array (
      ),
      2 => 
      array (
      ),
    ),
  ),
);
