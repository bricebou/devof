<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_truc' => 'Ajouter ce truc',

	// C
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_truc' => 'Confirmez-vous la suppression de cet truc ?',

	// I
	'icone_creer_truc' => 'Créer un truc',
	'icone_modifier_truc' => 'Modifier ce truc',
	'info_1_truc' => 'Un truc',
	'info_aucun_truc' => 'Aucun truc',
	'info_nb_trucs' => '@nb@ trucs',
	'info_trucs_auteur' => 'Les trucs de cet auteur',

	// R
	'retirer_lien_truc' => 'Retirer ce truc',
	'retirer_tous_liens_trucs' => 'Retirer tous les trucs',

	// S
	'supprimer_truc' => 'Supprimer cet truc',

	// T
	'texte_ajouter_truc' => 'Ajouter un truc',
	'texte_changer_statut_truc' => 'Ce truc est :',
	'texte_creer_associer_truc' => 'Créer et associer un truc',
	'texte_definir_comme_traduction_truc' => 'Ce truc est une traduction du truc numéro :',
	'titre_langue_truc' => 'Langue de ce truc',
	'titre_logo_truc' => 'Logo de ce truc',
	'titre_objets_lies_truc' => 'Liés à ce truc',
	'titre_page_trucs' => 'Les trucs',
	'titre_truc' => 'Truc',
	'titre_trucs' => 'Trucs',
	'titre_trucs_rubrique' => 'Trucs de la rubrique',
];
