<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_chose' => 'Ajouter cette chose',

	// C
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_chose' => 'Confirmez-vous la suppression de cette chose ?',

	// I
	'icone_creer_chose' => 'Créer une chose',
	'icone_modifier_chose' => 'Modifier cette chose',
	'info_1_chose' => 'Une chose',
	'info_aucun_chose' => 'Aucune chose',
	'info_choses_auteur' => 'Les choses de cet auteur',
	'info_nb_choses' => '@nb@ choses',

	// R
	'retirer_lien_chose' => 'Retirer cette chose',
	'retirer_tous_liens_choses' => 'Retirer toutes les choses',

	// S
	'supprimer_chose' => 'Supprimer cette chose',

	// T
	'texte_ajouter_chose' => 'Ajouter une chose',
	'texte_changer_statut_chose' => 'Cette chose est :',
	'texte_creer_associer_chose' => 'Créer et associer une chose',
	'texte_definir_comme_traduction_chose' => 'Cette chose est une traduction de la chose numéro :',
	'titre_chose' => 'Chose',
	'titre_choses' => 'Choses',
	'titre_choses_rubrique' => 'Choses de la rubrique',
	'titre_langue_chose' => 'Langue de cette chose',
	'titre_logo_chose' => 'Logo de cette chose',
	'titre_objets_lies_chose' => 'Liés à cette chose',
	'titre_page_choses' => 'Les choses',
];
