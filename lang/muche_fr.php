<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_muche' => 'Ajouter ce muche',

	// C
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_muche' => 'Confirmez-vous la suppression de cet muche ?',

	// I
	'icone_creer_muche' => 'Créer un muche',
	'icone_modifier_muche' => 'Modifier ce muche',
	'info_1_muche' => 'Un muche',
	'info_aucun_muche' => 'Aucun muche',
	'info_muches_auteur' => 'Les muches de cet auteur',
	'info_nb_muches' => '@nb@ muches',

	// R
	'retirer_lien_muche' => 'Retirer ce muche',
	'retirer_tous_liens_muches' => 'Retirer tous les muches',

	// S
	'supprimer_muche' => 'Supprimer cet muche',

	// T
	'texte_ajouter_muche' => 'Ajouter un muche',
	'texte_changer_statut_muche' => 'Ce muche est :',
	'texte_creer_associer_muche' => 'Créer et associer un muche',
	'texte_definir_comme_traduction_muche' => 'Ce muche est une traduction du muche numéro :',
	'titre_langue_muche' => 'Langue de ce muche',
	'titre_logo_muche' => 'Logo de ce muche',
	'titre_muche' => 'Muche',
	'titre_muches' => 'Muches',
	'titre_muches_rubrique' => 'Muches de la rubrique',
	'titre_objets_lies_muche' => 'Liés à ce muche',
	'titre_page_muches' => 'Les muches',
];
