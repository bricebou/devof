<?php

/**
 * Déclarations relatives à la base de données
 *
 * @plugin     La Fabrique - Test objet-enfant
 * @copyright  2024
 * @author     bricebou
 * @licence    GNU/GPL
 * @package    SPIP\Devof\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function devof_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['trucs'] = 'trucs';
	$interfaces['table_des_tables']['muches'] = 'muches';
	$interfaces['table_des_tables']['choses'] = 'choses';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function devof_declarer_tables_objets_sql($tables) {

	$tables['spip_trucs'] = [
		'type' => 'truc',
		'principale' => 'oui',
		'field' => [
			'id_truc'            => 'bigint(21) NOT NULL',
			'id_rubrique'        => 'bigint(21) NOT NULL DEFAULT 0',
			'titre'              => 'varchar(50) NOT NULL DEFAULT ""',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'        => 'id_truc',
			'KEY id_rubrique'    => 'id_rubrique',
			'KEY statut'         => 'statut',
		],
		'titre' => 'titre AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => ['titre', 'id_rubrique'],
		'champs_versionnes' => ['titre', 'id_rubrique'],
		'rechercher_champs' => [],
		'tables_jointures'  => [],
		'statut_textes_instituer' => [
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		],
		'statut' => [
			[
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => ['statut','tout']
			]
		],
		'texte_changer_statut' => 'truc:texte_changer_statut_truc',
		'parent' => [
			[ 'type'  => 'rubrique', 'champ' => 'id_rubrique' ]
		],
	];

	$tables['spip_muches'] = [
		'type' => 'muche',
		'principale' => 'oui',
		'field' => [
			'id_muche'           => 'bigint(21) NOT NULL',
			'id_truc'            => 'bigint(21) NOT NULL DEFAULT 0',
			'titre'              => 'varchar(50) NOT NULL DEFAULT ""',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'        => 'id_muche',
			'KEY id_truc'        => 'id_truc',
			'KEY statut'         => 'statut',
		],
		'titre' => 'titre AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => ['titre', 'id_truc'],
		'champs_versionnes' => ['titre', 'id_truc'],
		'rechercher_champs' => [],
		'tables_jointures'  => [],
		'statut_textes_instituer' => [
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		],
		'statut' => [
			[
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => ['statut','tout']
			]
		],
		'texte_changer_statut' => 'muche:texte_changer_statut_muche',
		'parent' => [
			[ 'type'  => 'truc', 'champ' => 'id_truc' ]
		],
	];

	$tables['spip_choses'] = [
		'type' => 'chose',
		'principale' => 'oui',
		'field' => [
			'id_chose'           => 'bigint(21) NOT NULL',
			'id_truc'            => 'bigint(21) NOT NULL DEFAULT 0',
			'titre'              => 'varchar(25) NOT NULL DEFAULT ""',
			'maj'                => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		],
		'key' => [
			'PRIMARY KEY'        => 'id_chose',
			'KEY id_truc'        => 'id_truc',
		],
		'titre' => 'titre AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => ['titre', 'id_truc'],
		'champs_versionnes' => ['titre', 'id_truc'],
		'rechercher_champs' => [],
		'tables_jointures'  => [],
		'parent' => [
			[ 'type'  => 'truc', 'champ' => 'id_truc' ]
		],
	];

	return $tables;
}
