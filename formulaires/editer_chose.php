<?php

/**
 * Gestion du formulaire de d'édition de chose
 *
 * @plugin     La Fabrique - Test objet-enfant
 * @copyright  2024
 * @author     bricebou
 * @licence    GNU/GPL
 * @package    SPIP\Devof\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Déclaration des saisies de chose
 *
 * @param int|string $id_chose
 *     Identifiant du chose. 'new' pour un nouveau chose.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un chose source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du chose, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array[]
 *     Saisies du formulaire
 */
function formulaires_editer_chose_saisies_dist($id_chose = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$saisies = [
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'titre',
				 'obligatoire' => 'oui',
				'label' => _T('chose:champ_titre_label'),
				
				
			],
		],
		[
			'saisie' => 'trucs',
			'options' => [
				'nom' => 'id_truc',
				'obligatoire' => 'oui',
				'label' => _T('truc:titre_truc'),
			],
		],


	];
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_chose
 *     Identifiant du chose. 'new' pour un nouveau chose.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un chose source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du chose, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_chose_identifier_dist($id_chose = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return json_encode([intval($id_chose)]);
}

/**
 * Chargement du formulaire d'édition de chose
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_chose
 *     Identifiant du chose. 'new' pour un nouveau chose.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un chose source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du chose, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_chose_charger_dist($id_chose = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('chose', $id_chose, $id_truc, $lier_trad, $retour, $config_fonc, $row, $hidden);
	if (!$valeurs['id_truc']) {
		$valeurs['id_truc'] = $id_truc;
	}
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de chose
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_chose
 *     Identifiant du chose. 'new' pour un nouveau chose.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un chose source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du chose, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_chose_verifier_dist($id_chose = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {

	$erreurs = formulaires_editer_objet_verifier('chose', $id_chose, ['titre', 'id_truc']);


	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de chose
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_chose
 *     Identifiant du chose. 'new' pour un nouveau chose.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un chose source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du chose, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_chose_traiter_dist($id_chose = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$retours = formulaires_editer_objet_traiter('chose', $id_chose, $id_truc, $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
