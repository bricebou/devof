<?php

/**
 * Gestion du formulaire de d'édition de muche
 *
 * @plugin     La Fabrique - Test objet-enfant
 * @copyright  2024
 * @author     bricebou
 * @licence    GNU/GPL
 * @package    SPIP\Devof\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Déclaration des saisies de muche
 *
 * @param int|string $id_muche
 *     Identifiant du muche. 'new' pour un nouveau muche.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un muche source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du muche, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array[]
 *     Saisies du formulaire
 */
function formulaires_editer_muche_saisies_dist($id_muche = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$saisies = [
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'titre',
				
				'label' => _T('muche:champ_titre_label'),
				
				
			],
		],
		[
			'saisie' => 'trucs',
			'options' => [
				'nom' => 'id_truc',
				'obligatoire' => 'oui',
				'label' => _T('truc:titre_truc'),
			],
		],


	];
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_muche
 *     Identifiant du muche. 'new' pour un nouveau muche.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un muche source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du muche, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_muche_identifier_dist($id_muche = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return json_encode([intval($id_muche)]);
}

/**
 * Chargement du formulaire d'édition de muche
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_muche
 *     Identifiant du muche. 'new' pour un nouveau muche.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un muche source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du muche, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_muche_charger_dist($id_muche = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('muche', $id_muche, $id_truc, $lier_trad, $retour, $config_fonc, $row, $hidden);
	if (!$valeurs['id_truc']) {
		$valeurs['id_truc'] = $id_truc;
	}
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de muche
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_muche
 *     Identifiant du muche. 'new' pour un nouveau muche.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un muche source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du muche, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_muche_verifier_dist($id_muche = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {

	$erreurs = formulaires_editer_objet_verifier('muche', $id_muche, ['id_truc']);


	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de muche
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_muche
 *     Identifiant du muche. 'new' pour un nouveau muche.
 * @param int $id_truc
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un muche source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du muche, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_muche_traiter_dist($id_muche = 'new', $id_truc = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$retours = formulaires_editer_objet_traiter('muche', $id_muche, $id_truc, $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
